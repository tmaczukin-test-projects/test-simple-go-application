package main

import (
	"fmt"

	"gitlab.com/tmaczukin-test-projects/test-simple-go-application/version"
)

func main() {
	info, err := version.GetVersion().ExtendedInfo()
	if err != nil {
		panic(err)
	}

	fmt.Println(info)
}
